import React from 'react';
import Select from 'arui-feather/select';

const OptionInput = ({ handler, touched, hasError, meta }) => (
  <div class="item">
    <label>{meta.label}</label>
    <Select
          size={ 's' }
          mode='radio'
          options={ meta.options }
      />
  </div>  
);

export default OptionInput;
import React from 'react';
import Input from 'arui-feather/input';

const TextInput = ({ handler, touched, hasError, meta }) => (
  <div>
    <label>{meta.label}</label>
    <Input  {...handler()} />
    <span>
        {touched
        && hasError("required")
        && `${meta.label} is required`}
    </span>
  </div>  
);

export default TextInput;
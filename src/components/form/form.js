import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  FormBuilder,
  FieldGroup,
  FieldControl,
  Validators,
} from "react-reactive-form";
import TextInput from './input';
import OptionInput from './option';
import { updateUserApi, cancelUpdateApi } from '../../redux/actions';
import { getLoading, getResult } from '../../redux/selector';
import FormAlfa from 'arui-feather/form';
import FormField from 'arui-feather/form-field';

class Form extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    this.cancle = {};
    this.state = {
      fields: {
        option : OptionInput,
        input : TextInput
      },
      dispatch,
      result : null
    };
    const fields = {};
    this.props.fields.map(field => {
      fields[field.name] = [(typeof field.values === 'object') ? field.values.value :  field.values, Validators.required];
    });
   
    this.loginForm = FormBuilder.group(fields);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cancel = this.cancel.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    console.log('Form values', this.loginForm.value);
    this.state.dispatch(updateUserApi(this.loginForm.value, this.cancle));
  }
  cancel() {
    this.state.dispatch(cancelUpdateApi(this.cancle));
  }
  render() {
    const { isFetching, result } = this.props
    console.log('result', result);
    return (
      <div class="form">
        <h2>{this.props.title}</h2>
        <FieldGroup
          control={this.loginForm}
          render={({ get, invalid }) => (
            <FormAlfa onSubmit={this.handleSubmit}>
              {
                this.props.fields.map(field => 
                  <FormField>
                  <FieldControl
                    name={field.name}
                    render={this.state.fields[field.type]}
                    meta={{ label: field.title, options: field.values}}
                  />
                  </FormField>
                )
              }
              <Button view='extra' disabled={invalid} type='submit'>Submit</Button>
            </FormAlfa>
          )}
        />
      {
        isFetching && <div>Loading... <button onClick={this.cancel}  >Cancel</button></div>
      }
      {
        result && Object.keys(result).map(key => (
         <div>{key} : {result[key]}</div>
        ))
      }
      </div>
    );
  }

}
function mapStateToProps(state) {
  const result = getResult(state);
  const isFetching = getLoading(state);
  return {
    isFetching,
    result
  }
}
export default connect(mapStateToProps)(Form)

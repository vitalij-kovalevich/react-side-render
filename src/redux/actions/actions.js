import axios from 'axios';
import axiosCancel from 'axios-cancel';

export const REQUEST_APPS = 'REQUEST_APPS';
export const RECEIVE_APPS = 'RECEIVE_APPS';

export const UPDATE_USER = '[User] update user';
export const CANCEL_UPDATE_USER = '[User] cancel update user';
export const SUCCESS_UPDATE_USER = '[User] success update user';
export const FAIL_UPDATE_USER = '[User] fail update user'

axiosCancel(axios, {
  debug: false // default
});

function requestApps() {
  return {
    type: REQUEST_APPS
  }
}
function updateUser(data, id) {
  return {
    type: UPDATE_USER,
    data,
    id
  }
}
function cancelUpdateUser(id) {
  return {
    type: CANCEL_UPDATE_USER,
    id
  }
}
function successUpdateUser(data) {
  return {
    type: SUCCESS_UPDATE_USER,
    data : data
  }
}
function failUpdateUser() {
  return {
    type: FAIL_UPDATE_USER
  }
}

function receiveApps(json) {
  return {
    type: RECEIVE_APPS,
    apps: json
  }
}

function fetchApps() {
  return dispatch => {
    dispatch(requestApps())
    return fetch(`assets/data.json`)
      .then(response => response.json())
      .then(json => dispatch(receiveApps(json)))
  }
}
function fetchUpdateUser(data, cancel) {
  return dispatch => {
    dispatch(updateUser());
    const CancelToken = axios.CancelToken;
    cancel.source = CancelToken.source();
    axios.put(`/api/user`, data, {
      cancelToken: cancel.source.token
    })
      .then(response => response.data)
      .then(json => dispatch(successUpdateUser(json)))
      .catch(err => {
        if (axios.isCancel(thrown)) {
         
        } else {
          return dispatch(failUpdateUser());
        }
      })
  }
}

function shouldFetchApps(state) {
  const apps = state.apps
  if (apps.length==0) {
    return true
  } else if (state.isFetching) {
    return false
  }
}

export function fetchAppsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchApps(getState())) {
      return dispatch(fetchApps())
    }
  }
}

export function updateUserApi(data, id) {
  return (dispatch, getState) => {
    return dispatch(fetchUpdateUser(data, id))
  }
}

export function cancelUpdateApi(cancel){
  return (dispatch) => {
    cancel.source.cancel();
    return dispatch(cancelUpdateUser())
  }
}
